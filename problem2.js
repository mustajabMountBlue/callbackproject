/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
// Importing Dependencies
const fs = require("fs");

// Read the given file and return its contents as a string
function readFile(filepath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, "utf8", (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

// Read the given file and delete its contents file
function deleteFiles(filepath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, "utf8", (err, data) => {
      if (err) {
        reject(err);
      } else {
        const files = data.split("\n");
        files.pop();
        files.forEach((file) => {
          fs.unlink(file, (err) => {
            if (err) console.error(err);
          });
        });
        resolve();
      }
    });
  });
}

// Write the given data to a file
function writeFile(filepath, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(filepath, data, "utf8", (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

// Appending the given name to a file
function appendFile(filepath, data) {
  return new Promise((resolve, reject) => {
    fs.appendFile(filepath, data, "utf8", (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

// Convert the given string to uppercase
function toUppercase(str) {
  return str.toUpperCase();
}

// Convert the given string to lowercase
function toLowercase(str) {
  return str.toLowerCase();
}

// Split the given string into sentences
function splitIntoSentences(str) {
  return str.split(". ");
}

// Sort the lines in the given string
function sortLines(str) {
  return str.split("\n").sort().join("\n");
}

// Read the "lipsum.txt" file, convert the contents to uppercase, and write the
// result to a new file called "uppercase.txt"
module.exports = (file) => {
  readFile(file)
    .then((data) => {
      const uppercaseData = toUppercase(data);
      return writeFile("uppercase.txt", uppercaseData);
    })
    .then(() => {
      // Write the name of the new file to "filenames.txt"
      return appendFile("filenames.txt", "uppercase.txt\n");
    })
    .then(() => {
      // Read the "uppercase.txt" file, convert the contents to lowercase, split the
      // contents into sentences, and write the result to a new file called
      // "lowercase.txt"
      return readFile("uppercase.txt");
    })
    .then((data) => {
      const lowercaseData = toLowercase(data);
      const sentences = splitIntoSentences(lowercaseData);
      return writeFile("lowercase.txt", sentences.join("\n"));
    })
    .then(() => {
      // Write the name of the new file to "filenames.txt"
      return appendFile("filenames.txt", "lowercase.txt\n");
    })
    .then(() => {
      // Read the "lowercase.txt"
      return readFile("lowercase.txt");
    })
    .then((data) => {
      const sortedData = sortLines(data);
      return writeFile("sorted.txt", sortedData);
    })
    .then(() => {
      // Write the name of the new file to "filenames.txt"
      return appendFile("filenames.txt", "sorted.txt\n");
    })
    .then(() => {
      return deleteFiles("filenames.txt");
    });
};
