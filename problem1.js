const fs = require("fs");
const path = require("path");
const randomDir = path.join(__dirname, "/random");

// Create a directory of random JSON files
function createJSONFiles() {
  return new Promise((resolve, reject) => {
    try {
      fs.mkdir(randomDir, (err) => {
        if (err) throw err;
        for (let i = 0; i < 5; i++) {
          fs.writeFile(
            `${randomDir}/file_${i}.json`,
            JSON.stringify({ a: 1, b: 2, c: 3 }),
            (err) => {
              if (err) throw err;
            }
          );
        }
        resolve();
      });
    } catch (error) {
      reject(error);
    }
  });
}

// Delete the JSON files in the directory
function deleteJSONFiles() {
  return new Promise((resolve, reject) => {
    try {
      fs.readdir(randomDir, (err, files) => {
        if (err) throw err;
        files.forEach((file) => {
          fs.unlink(`${randomDir}/${file}`, (err) => {
            if (err) throw err;
          });
        });
        resolve();
      });
    } catch (error) {
      reject(error);
    }
  });
}

createJSONFiles()
  .then(() => {
    console.log("JSON files created successfully");
    return deleteJSONFiles();
  })
  .then(() => {
    console.log("JSON files deleted successfully");
  })
  .catch((error) => {
    console.error(error);
  });
